import { connect } from 'react-redux'

export default Comp => {
  const mapStateToProps = ({ platform }) => ({ platform })
  return connect(mapStateToProps)(Comp)
}
