import { createSelector } from 'reselect'

const getTicker = (state, { ticker }) => ticker

const getSelectedTickers = ({ userSettings: { selectedTickers } }) =>
  selectedTickers

export default createSelector(
  [getTicker, getSelectedTickers],
  (ticker, selectedTickers) => {
    const index = selectedTickers.indexOf(ticker.id)
    return {
      canMoveUp: index > 0,
      canMoveDown: index < selectedTickers.length - 1
    }
  }
)
