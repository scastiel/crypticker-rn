import { createSelector } from 'reselect'

const getTicker = (state, { ticker }) => ticker

const getCurrencies = ({ referenceData: { currencies } }) => currencies

export default createSelector(
  [getTicker, getCurrencies],
  (ticker, currencies) => ({
    ...ticker,
    currFrom: currencies.find(currency => currency.id === ticker.currFrom),
    currTo: currencies.find(currency => currency.id === ticker.currTo)
  })
)
