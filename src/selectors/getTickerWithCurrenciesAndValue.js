import { createSelector } from 'reselect'
import getTickerWithCurrencies from './getTickerWithCurrencies'

const getValue = ({ values: { tickers } }) => tickers

export default createSelector(
  [getTickerWithCurrencies, getValue],
  (ticker, values) => ({
    ...ticker,
    value: values[ticker.id]
  })
)
