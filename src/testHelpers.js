export function getInnerText(element) {
  return element.prop('children')
}

export function textAt(element, selector = null) {
  return getInnerText(selector ? element.find(selector) : element)
}
