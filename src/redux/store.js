import { periodicRefresh } from '../services/gui/actions'
import { addTickers } from '../services/userSettings/actions'
import { initPlatform } from '../services/platform/actions'
import { getCurrenciesAndTickers } from '../services/referenceData/actions'
import { createStore, applyMiddleware, combineReducers } from 'redux'
import createSagaMiddleware from 'redux-saga'
import * as storage from 'redux-storage'
import createEngine from 'redux-storage-engine-reactnativeasyncstorage'
import filter from 'redux-storage-decorator-filter'
import rootSaga from './sagas'
import reducers from './reducers'
import { CURRENT_REFERENCE_DATA_VERSION } from '../services/referenceData/reducer'

const DEFAULT_TICKERS = ['btcusd', 'ethusd', 'ethbtc']

export default (additionnalReducers = {}) => {
  const reducer = storage.reducer(
    combineReducers({
      ...reducers,
      ...additionnalReducers
    })
  )
  const storageEngine = filter(createEngine('my-save-key'), null, [
    ['nav'],
    ['gui'],
    ['values', 'requestingTickers'],
    ['values', 'isManual']
  ])

  const storageMiddleWare = storage.createMiddleware(storageEngine)

  const sagaMiddleware = createSagaMiddleware()

  const store = createStore(
    reducer,
    applyMiddleware(sagaMiddleware, storageMiddleWare)
  )

  const load = storage.createLoader(storageEngine)
  load(store).then(state => {
    store.dispatch(initPlatform())
    const userSettings = state.userSettings
    const selectedTickers = userSettings ? userSettings.selectedTickers : []
    const referenceData = state.referenceData
    const currencies = referenceData ? referenceData.currencies : []
    const tickers = referenceData ? referenceData.tickers : []
    const version = referenceData ? referenceData.version || 1 : 1
    if (version < CURRENT_REFERENCE_DATA_VERSION) {
      store.dispatch(addTickers(DEFAULT_TICKERS))
      store.dispatch(getCurrenciesAndTickers())
    }
    if (currencies.length === 0 || tickers.length === 0) {
      store.dispatch(getCurrenciesAndTickers())
    }
    if (selectedTickers.length === 0) {
      store.dispatch(addTickers(DEFAULT_TICKERS))
    }

    store.dispatch(periodicRefresh())
  })

  sagaMiddleware.run(rootSaga)

  return store
}
