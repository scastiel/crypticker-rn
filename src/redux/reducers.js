import gui from '../services/gui/reducer'
import userSettings from '../services/userSettings/reducer'
import referenceData from '../services/referenceData/reducer'
import values from '../services/values/reducer'
import platform from '../services/platform/reducer'

export default { gui, userSettings, referenceData, values, platform }
