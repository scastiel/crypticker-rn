import { all } from 'redux-saga/effects'
import gui from '../services/gui/sagas'
import referenceData from '../services/referenceData/sagas'
import values from '../services/values/sagas'
import platform from '../services/platform/sagas'

export default function*() {
  yield all([gui(), referenceData(), values(), platform()])
}
