import { StyleSheet } from 'react-native'

export const primaryColor = 'rgb(3, 169, 244)'
export const primaryColorAccent = 'rgb(32, 177, 243)'
export const secondaryColor = 'rgb(255, 64, 129)'
export const fontFamily = 'roboto'

export default StyleSheet.create({
  list: {
    backgroundColor: 'rgb(250, 250, 250)'
  },
  tickerListItem: {
    width: '100%',
    borderBottomWidth: 1,
    borderStyle: 'solid',
    borderColor: '#e3e3e3',
    borderRadius: 2,
    padding: 16,
    backgroundColor: 'white',
    display: 'flex',
    alignItems: 'flex-end',
    alignSelf: 'center'
  },
  actions: {
    display: 'flex',
    flexDirection: 'row',
    alignContent: 'space-between',
    padding: 5,
    marginBottom: -12,
    marginRight: -12
  },
  actionButton: {
    padding: 7,
    marginLeft: 12
  },
  refreshActionButton: {
    paddingTop: 12,
    alignItems: 'center'
  },
  actionButtonText: {
    fontFamily: fontFamily
  },
  primaryActionButtonText: {
    color: primaryColor
  },
  secondaryActionButtonText: {
    color: secondaryColor
  },
  sectionHeader: {
    padding: 12,
    backgroundColor: 'rgb(250, 250, 250)'
  },
  sectionHeaderTitle: {
    color: 'darkgray',
    fontFamily: fontFamily,
    fontSize: 18
  },
  listFooter: {
    marginTop: 12,
    marginBottom: 12
  },
  listFooterText: {
    color: 'darkgray',
    fontSize: 14,
    textAlign: 'center',
    fontFamily: fontFamily
  },
  header: {
    backgroundColor: primaryColor
  }
})
