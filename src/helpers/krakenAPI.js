import { fetchAssets, fetchPairs, fetchAveragePrices } from 'cryptowatchjs'

export async function fetchTickers(selectedTickers) {
  return await fetchAveragePrices()
}

export async function fetchCurrenciesAndAvailableTickers() {
  const [currencies, availableTickers] = await Promise.all([
    fetchCurrencies(),
    fetchAvailableTickers()
  ])
  return { currencies, availableTickers }
}

function assetToCurrency(asset) {
  return { id: asset.symbol, symbol: asset.symbol.toUpperCase() }
}

function pairToTicker(pair) {
  return {
    id: pair.symbol,
    currFrom: pair.base.symbol,
    currTo: pair.quote.symbol,
    value: null
  }
}

async function fetchCurrencies() {
  const assets = await fetchAssets()
  return assets.map(assetToCurrency)
}

async function fetchAvailableTickers() {
  const pairs = await fetchPairs()
  return pairs.filter(pair => !pair.futuresContractPeriod).map(pairToTicker)
}
