import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Ticker from './Ticker'
import ActionButton from './ActionButton'
import { View } from 'react-native'
import styles from '../styles'
import { connect } from 'react-redux'
import {
  moveUpTicker,
  moveDownTicker,
  removeTicker
} from '../services/userSettings/actions'
import getTickerMovabilityInfo from '../selectors/getTickerMovabilityInfo'
import tickerShape from './shapes/ticker'

export class TickerItem extends Component {
  static propTypes = {
    ticker: PropTypes.shape(tickerShape).isRequired,
    editMode: PropTypes.bool,
    canMoveUp: PropTypes.bool,
    canMoveDown: PropTypes.bool,
    removeTicker: PropTypes.func.isRequired,
    moveUpTicker: PropTypes.func.isRequired,
    moveDownTicker: PropTypes.func.isRequired
  }
  render() {
    const {
      ticker,
      editMode,
      canMoveUp,
      canMoveDown,
      moveUpTicker,
      moveDownTicker,
      removeTicker
    } = this.props
    return (
      <View style={styles.tickerListItem}>
        <Ticker ticker={ticker} />
        {editMode && (
          <View style={styles.actions}>
            {canMoveUp && (
              <ActionButton
                className="move-up-button"
                title="Move up"
                onPress={() => moveUpTicker(ticker.id)}
              />
            )}
            {canMoveDown && (
              <ActionButton
                className="move-down-button"
                title="Move down"
                onPress={() => moveDownTicker(ticker.id)}
              />
            )}
            <ActionButton
              className="remove-button"
              title="Remove"
              onPress={() => removeTicker(ticker.id)}
              isSecondary={true}
            />
          </View>
        )}
      </View>
    )
  }
}

const mapStateToProps = (state, props) => ({
  ...getTickerMovabilityInfo(state, props)
})

const mapDispatchToProps = { moveUpTicker, moveDownTicker, removeTicker }

export default connect(mapStateToProps, mapDispatchToProps)(TickerItem)
