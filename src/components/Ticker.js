import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { StyleSheet, Text } from 'react-native'
import { fontFamily } from '../styles'
import { connect } from 'react-redux'
import getTickerWithCurrenciesAndValue from '../selectors/getTickerWithCurrenciesAndValue'
import tickerWithCurrenciesAndValueShape from './shapes/tickerWithCurrenciesAndValue'
import platformAware from '../hoc/platformAware'

const styles = StyleSheet.create({
  ticker: {
    paddingTop: 3,
    paddingBottom: 3,
    textAlign: 'right',
    fontSize: 20,
    color: 'darkgray',
    fontFamily
  },
  tickerValue: {
    color: 'black',
    fontSize: 24
  },
  tickerOne: {
    fontSize: 24
  }
})

export class Ticker extends Component {
  static propTypes = {
    ticker: PropTypes.shape(tickerWithCurrenciesAndValueShape),
    platform: PropTypes.object.isRequired
  }
  formatNumber(number) {
    if (this.props.platform.os === 'ios') {
      return number.toLocaleString(undefined, {
        minimumSignificantDigits: 3,
        maximumSignificantDigits: 3
      })
    } else {
      const n = number.toPrecision(3)
      return n.includes('e') ? String(parseFloat(n)) : n
    }
  }
  render() {
    const value = this.props.ticker.value
    return (
      <Text className="ticker" style={styles.ticker}>
        <Text className="one" style={styles.tickerOne}>
          1
        </Text>{' '}
        <Text className="currency-from-symbol">
          {this.props.ticker.currFrom.symbol}
        </Text>{' '}
        <Text className="equal">=</Text>{' '}
        <Text className="ticker-value" style={styles.tickerValue}>
          {typeof value === 'number' ? this.formatNumber(value) : '…'}
        </Text>{' '}
        <Text className="currency-to-symbol">
          {this.props.ticker.currTo.symbol}
        </Text>
      </Text>
    )
  }
}

const mapStateToProps = (state, props) => ({
  ticker: getTickerWithCurrenciesAndValue(state, props)
})

export default platformAware(connect(mapStateToProps)(Ticker))
