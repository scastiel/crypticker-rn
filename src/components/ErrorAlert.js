import React, { Component } from 'react'
import { TouchableHighlight, Text, StyleSheet } from 'react-native'
import PropTypes from 'prop-types'

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    bottom: 0,
    left: 12,
    right: 12,
    backgroundColor: 'rgba(0, 0, 0, 0.87)',
    padding: 12
  },
  text: { color: 'white' }
})

export default class ErrorAlert extends Component {
  static propTypes = {
    errorMessage: PropTypes.string.isRequired,
    hideError: PropTypes.func
  }
  render() {
    return (
      <TouchableHighlight
        style={styles.container}
        onPress={() => this.props.hideError && this.props.hideError()}
      >
        <Text style={styles.text}>{this.props.errorMessage}</Text>
      </TouchableHighlight>
    )
  }
}
