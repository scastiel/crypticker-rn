import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Platform, View, Text } from 'react-native'
import ActionButton from './ActionButton'
import styles from '../styles'
import { connect } from 'react-redux'
import { addTickerAndRefresh } from '../services/gui/actions'
import getTickerWithCurrencies from '../selectors/getTickerWithCurrencies'
import tickerWithCurrenciesShape from './shapes/tickerWithCurrencies'

export class AvailableTickerItem extends Component {
  static propTypes = {
    ticker: PropTypes.shape(tickerWithCurrenciesShape).isRequired,
    addTickerAndRefresh: PropTypes.func.isRequired
  }
  render() {
    return (
      <View
        style={[
          styles.tickerListItem,
          { flexDirection: 'row', justifyContent: 'space-between' }
        ]}
      >
        <Text
          className="ticker-symbol"
          style={{ fontSize: 20, color: 'darkgray' }}
        >
          {this.props.ticker.currFrom.symbol +
            ' → ' +
            this.props.ticker.currTo.symbol}
        </Text>
        <View
          style={[
            styles.actions,
            {
              marginBottom: Platform.OS === 'android' ? -10 : -12,
              marginTop: -12
            }
          ]}
        >
          <ActionButton
            className="add-button"
            title="Add"
            onPress={() => this.props.addTickerAndRefresh(this.props.ticker.id)}
          />
        </View>
      </View>
    )
  }
}

const mapStateToProps = (state, props) => ({
  ticker: getTickerWithCurrencies(state, props)
})
const mapDispatchToProps = { addTickerAndRefresh }
export default connect(mapStateToProps, mapDispatchToProps)(AvailableTickerItem)
