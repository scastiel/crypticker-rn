import PropTypes from 'prop-types'
import ticker from './ticker'

export default {
  ...ticker,
  currFrom: PropTypes.shape({
    id: PropTypes.string.isRequired,
    symbol: PropTypes.string.isRequired
  }).isRequired,
  currTo: PropTypes.shape({
    id: PropTypes.string.isRequired,
    symbol: PropTypes.string.isRequired
  }).isRequired
}
