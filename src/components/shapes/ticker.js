import PropTypes from 'prop-types'

export default {
  id: PropTypes.string.isRequired,
  currFrom: PropTypes.string.isRequired,
  currTo: PropTypes.string.isRequired
}
