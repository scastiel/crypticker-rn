import PropTypes from 'prop-types'
import tickerWithCurrencies from './tickerWithCurrencies'

export default {
  ...tickerWithCurrencies,
  value: PropTypes.number
}
