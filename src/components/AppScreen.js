import React, { Component } from 'react'
import { Text, View, TouchableHighlight } from 'react-native'
import TickerApp from './TickerApp'
import styles, { primaryColor, fontFamily } from '../styles'

export default class AppScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const { state, setParams } = navigation
    const { params } = state
    return {
      headerTitle: (
        <TouchableHighlight
          onPress={() => navigation.navigate('About')}
          underlayColor={primaryColor}
          style={{ paddingLeft: 12, paddingRight: 12 }}
        >
          <Text
            style={{
              color: 'white',
              fontFamily,
              fontSize: 22
            }}
          >
            Crypticker
          </Text>
        </TouchableHighlight>
      ),
      headerTintColor: 'white',
      headerStyle: styles.header,
      headerRight: (
        <View style={{ paddingRight: 9 }}>
          <TouchableHighlight
            style={{ padding: 10 }}
            underlayColor={primaryColor}
            onPress={() =>
              setParams({ editMode: !(params && params.editMode) })
            }
          >
            <Text style={{ color: 'white', fontFamily }}>
              {params && params.editMode ? 'Done' : 'Edit'}
            </Text>
          </TouchableHighlight>
        </View>
      )
    }
  }

  render() {
    const params = this.props.navigation.state.params
    return <TickerApp editMode={params && params.editMode} />
  }
}
