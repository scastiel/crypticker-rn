import React, { Component } from 'react'
import { WebView, Linking } from 'react-native'
import { fontFamily, primaryColor } from '../styles'
import packageJson from '../../package.json'

const html = `
<!doctype html>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1">
<style>
  body {
    background-color: rgb(250, 250, 250);
    font-family: ${fontFamily}, sans-serif;
    color: #666;
  }
  a {
    color: ${primaryColor};
  }
  a:visited {
    color: ${primaryColor};
  }
</style>

<p>
  <a href="https://crypticker.io" target="_blank">Crypticker</a> is open
  source and distributed under the terms of the
  <a href="https://www.gnu.org/licenses/gpl-3.0.en.html" target="_blank">GNU GPL v3</a>
  licence.
</p>
<p>
  It's built with:
</p>
<ul>
  <li><a href="https://facebook.github.io/react/" target="_blank">React</a></li>
  <li><a href="https://facebook.github.io/react-native/" target="_blank">React Native</a></li>
  <li><a href="https://github.com/react-community/create-react-native-app" target="_blank">create-react-native-app</a>
  <li><a href="https://reactnavigation.org" target="_blank">React Navigation</a></li>
  <li><a href="https://expo.io" target="_blank">Expo.io</a></li>
</ul>
<p>
  See the source and contribute on
  <a href="https://gitlab.com/scastiel/crypticker" target="_blank">GitLab.com</a>.
</p>
<p>
  Data is provided by
  <a href="https://cryptowat.ch/" target="_blank">Cryptowatch</a> API.
</p>
<p>
  <a href="https://twitter.com/crypticker_io" target="_blank">Find us on Twitter</a>!
</p>
<p>
  Written and maintained by
  <a href="https://twitter.com/scastiel" target="_blank">Sébastien Castiel</a>.
</p>
<p>
  Version ${packageJson.version}.
</p>
`

export default class AboutView extends Component {
  render() {
    return (
      <WebView
        source={{ html }}
        onShouldStartLoadWithRequest={req => {
          const isLocal = req.url === 'about:blank'
          if (isLocal) {
            return true
          } else {
            Linking.openURL(req.url)
            return false
          }
        }}
      />
    )
  }
}
