import React, { Component } from 'react'
import { Text } from 'react-native'
import { connect, Provider } from 'react-redux'
import configureStore from '../redux/store'
import { StackNavigator, addNavigationHelpers } from 'react-navigation'
import { Font } from 'expo'
import AppScreen from './AppScreen'
import AboutScreen from './AboutScreen'

const AppNavigator = StackNavigator({
  App: {
    screen: AppScreen
  },
  About: {
    screen: AboutScreen
  }
})

const initialState = AppNavigator.router.getStateForAction(
  AppNavigator.router.getActionForPathAndParams('App')
)

const navReducer = (state = initialState, action) => {
  const nextState = AppNavigator.router.getStateForAction(action, state)

  // Simply return the original `state` if `nextState` is null or undefined.
  return nextState || state
}

class App extends Component {
  render() {
    return (
      <AppNavigator
        navigation={addNavigationHelpers({
          dispatch: this.props.dispatch,
          state: this.props.nav
        })}
      />
    )
  }
}

const mapStateToProps = state => ({
  nav: state.nav
})

const AppWithNavigationState = connect(mapStateToProps)(App)

const store = configureStore({ nav: navReducer })

export default class extends Component {
  constructor() {
    super()
    this.state = { fontLoaded: false }
  }
  async componentDidMount() {
    await Font.loadAsync({
      roboto: require('../assets/roboto/Roboto-Regular.ttf')
    })
    this.setState({ fontLoaded: true })
  }
  render() {
    return this.state.fontLoaded ? (
      <Provider store={store}>
        <AppWithNavigationState />
      </Provider>
    ) : (
      <Text />
    )
  }
}
