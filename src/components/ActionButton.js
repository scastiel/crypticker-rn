import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { TouchableHighlight, Text } from 'react-native'
import styles from '../styles'

export default class ActionButton extends Component {
  static propTypes = {
    title: PropTypes.string.isRequired,
    onPress: PropTypes.func.isRequired,
    isSecondary: PropTypes.bool
  }
  render() {
    return (
      <TouchableHighlight
        underlayColor="white"
        style={styles.actionButton}
        onPress={this.props.onPress}
      >
        <Text
          style={[
            styles.actionButtonText,
            this.props.isSecondary
              ? styles.secondaryActionButtonText
              : styles.primaryActionButtonText
          ]}
        >
          {this.props.title.toUpperCase()}
        </Text>
      </TouchableHighlight>
    )
  }
}
