import React, { Component } from 'react'
import AboutView from './AboutView'
import styles, { fontFamily } from '../styles'

export default class AboutScreen extends Component {
  static navigationOptions = {
    title: 'About',
    headerTintColor: 'white',
    headerStyle: styles.header,
    headerTitleStyle: {
      color: 'white',
      fontFamily,
      fontWeight: 'normal',
      fontSize: 22
    }
  }

  render() {
    return <AboutView />
  }
}
