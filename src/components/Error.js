import React from 'react'
import { View } from 'react-native'
import { connect } from 'react-redux'
import ErrorAlert from './ErrorAlert'
import { hideError } from '../services/gui/actions'

const Error = ({ errorDisplayed, errorMessage, hideError }) =>
  errorDisplayed ? (
    <ErrorAlert errorMessage={errorMessage} hideError={hideError} />
  ) : (
    <View />
  )

const mapStateToProps = ({ gui: { errorDisplayed, errorMessage } }) => ({
  errorDisplayed,
  errorMessage
})

const mapDispatchToProps = { hideError }

export default connect(mapStateToProps, mapDispatchToProps)(Error)
