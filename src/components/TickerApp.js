import React, { Component } from 'react'
import { View, StatusBar } from 'react-native'
import PropTypes from 'prop-types'
import TickerListView from './TickerListView'
import Error from './Error'
import { primaryColor } from '../styles'

export default class TickerApp extends Component {
  static propTypes = {
    editMode: PropTypes.bool
  }
  render() {
    return (
      <View style={{ height: '100%' }}>
        <StatusBar barStyle="light-content" backgroundColor={primaryColor} />
        <TickerListView editMode={this.props.editMode} />
        <Error />
      </View>
    )
  }
}
