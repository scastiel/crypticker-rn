import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import TickerItem from './TickerItem'
import AvailableTickerItem from './AvailableTickerItem'
import { Text, View, SectionList } from 'react-native'
import ActionButton from './ActionButton'
import { refreshReferenceData } from '../services/referenceData/actions'
import { refresh } from '../services/values/actions'
import styles from '../styles'
import tickerShape from './shapes/ticker'

export class TickerListView extends Component {
  static propTypes = {
    editMode: PropTypes.bool,
    tickers: PropTypes.arrayOf(PropTypes.shape(tickerShape)).isRequired,
    selectedTickers: PropTypes.arrayOf(PropTypes.string).isRequired,
    refresh: PropTypes.func.isRequired,
    refreshReferenceData: PropTypes.func.isRequired,
    requestingTickers: PropTypes.bool,
    isManual: PropTypes.bool,
    lastUpdated: PropTypes.string
  }
  createItemForSelectedTicker(ticker) {
    return <TickerItem ticker={ticker} editMode={this.props.editMode} />
  }
  createItemForAvailableTicker(ticker) {
    return (
      <View style={{ paddingLeft: 12, paddingRight: 12 }}>
        <AvailableTickerItem ticker={ticker} />
      </View>
    )
  }
  createListFooter() {
    return this.props.lastUpdated ? (
      <View style={styles.listFooter}>
        <Text style={styles.listFooterText}>
          {'Last update: ' + new Date(this.props.lastUpdated).toLocaleString()}
        </Text>
      </View>
    ) : (
      <Text />
    )
  }
  getSections() {
    const displayedTickersSection = {
      key: 'displayed',
      data: this.props.selectedTickers
        .map(tickerId =>
          this.props.tickers.find(ticker => ticker.id === tickerId)
        )
        .filter(ticker => !!ticker),
      title: 'Displayed tickers',
      renderItem: ({ item: ticker }) => this.createItemForSelectedTicker(ticker)
    }
    const availableTickersSection = {
      key: 'available',
      data: this.props.tickers.filter(
        ticker => !this.props.selectedTickers.includes(ticker.id)
      ),
      title: 'Available tickers',
      renderItem: ({ item: ticker }) =>
        this.createItemForAvailableTicker(ticker)
    }
    const refreshReferenceData = {
      key: 'refresh',
      data: [1],
      title: 'Refresh available tickers',
      renderItem: () => (
        <View style={styles.refreshActionButton}>
          <ActionButton
            title="Touch to refresh"
            onPress={() => this.props.refreshReferenceData()}
          />
        </View>
      )
    }
    return this.props.editMode
      ? [displayedTickersSection, availableTickersSection, refreshReferenceData]
      : [displayedTickersSection]
  }
  render() {
    return (
      <SectionList
        style={styles.list}
        sections={this.getSections()}
        renderSectionHeader={({ section }) =>
          this.props.editMode ? (
            <View style={styles.sectionHeader}>
              <Text style={styles.sectionHeaderTitle}>{section.title}</Text>
            </View>
          ) : (
            <View style={{ height: 0 }} />
          )
        }
        ListFooterComponent={() => this.createListFooter()}
        keyExtractor={ticker => ticker.id}
        refreshing={!!this.props.requestingTickers && !!this.props.isManual}
        onRefresh={() => this.props.refresh(true)}
      />
    )
  }
}

const mapStateToProps = state => ({
  tickers: state.referenceData.tickers,
  selectedTickers: state.userSettings.selectedTickers,
  lastUpdated: state.values.lastUpdated,
  requestingTickers: state.values.requestingTickers,
  isManual: state.values.isManual
})

const mapDispatchToProps = {
  refreshReferenceData,
  refresh
}

export default connect(mapStateToProps, mapDispatchToProps)(TickerListView)
