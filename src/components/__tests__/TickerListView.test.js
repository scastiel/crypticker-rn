import React from 'react'
import TickerItem from '../TickerItem'
import AvailableTickerItem from '../AvailableTickerItem'
import ActionButton from '../ActionButton'
import { TickerListView } from '../TickerListView'
import { shallow } from 'enzyme'
import { SectionList } from 'react-native'
import { textAt } from '../../testHelpers'

const tickers = [
  {
    id: 'XXBTZUSD',
    currFrom: 'XXBT',
    currTo: 'ZUSD'
  },
  {
    id: 'XXBTZEUR',
    currFrom: 'XXBT',
    currTo: 'ZEUR'
  },
  {
    id: 'XETHZUSD',
    currFrom: 'XETH',
    currTo: 'ZUSD'
  },
  {
    id: 'XETHZEUR',
    currFrom: 'XETH',
    currTo: 'ZEUR'
  }
]
const selectedTickers = ['XXBTZUSD', 'XXBTZEUR']
const refresh = jest.fn()
const refreshReferenceData = jest.fn()

const defaultProps = { tickers, selectedTickers, refresh, refreshReferenceData }

describe('Component: TickerListView', () => {
  beforeEach(() => {
    refresh.mockClear()
    refreshReferenceData.mockClear()
  })

  it('should show last updated date if specified', () => {
    const wrapper = shallow(
      <TickerListView
        {...defaultProps}
        lastUpdated="2017-11-24T01:48:21.123Z"
      />
    )
    const ListFooterComponent = wrapper
      .find(SectionList)
      .prop('ListFooterComponent')
    const footerElement = shallow(<ListFooterComponent />)
    expect(textAt(footerElement, 'Text')).toEqual(
      'Last update: 2017-11-23 20:48:21'
    )
  })

  it('should not show last updated date if not specified', () => {
    const wrapper = shallow(
      <TickerListView {...defaultProps} lastUpdated={undefined} />
    )
    const ListFooterComponent = wrapper
      .find(SectionList)
      .prop('ListFooterComponent')
    const footerElement = shallow(<ListFooterComponent />)
    expect(textAt(footerElement)).toBeUndefined()
  })

  it('should display only tickers section if not edit mode', () => {
    const wrapper = shallow(<TickerListView {...defaultProps} />)
    const sections = wrapper.find(SectionList).prop('sections')
    expect(sections).toHaveLength(1)
    const displayedTickersSection = sections[0]
    expect(displayedTickersSection.data).toHaveLength(2)
    expect(displayedTickersSection.data[0]).toBe(tickers[0])
    expect(displayedTickersSection.data[1]).toBe(tickers[1])
    const renderedTickerItem = shallow(
      displayedTickersSection.renderItem({ item: tickers[0] })
    )
    expect(renderedTickerItem.find(TickerItem).prop('ticker')).toBe(tickers[0])
  })

  it('should display available tickers and refresh reference data sections if edit mode', () => {
    const wrapper = shallow(
      <TickerListView {...defaultProps} editMode={true} />
    )
    const sections = wrapper.find(SectionList).prop('sections')
    expect(sections).toHaveLength(3)

    const availableTickersSection = sections[1]
    expect(availableTickersSection.data).toHaveLength(2)
    expect(availableTickersSection.data[0]).toBe(tickers[2])
    expect(availableTickersSection.data[1]).toBe(tickers[3])
    const renderedAvailableTickerItem = shallow(
      availableTickersSection.renderItem({ item: tickers[2] })
    )
    expect(
      renderedAvailableTickerItem.find(AvailableTickerItem).prop('ticker')
    ).toBe(tickers[2])

    const refreshReferenceDataSection = sections[2]
    expect(refreshReferenceDataSection.data).toHaveLength(1)
    const renderedRefreshReferenceData = shallow(
      refreshReferenceDataSection.renderItem({ item: 1 })
    )
    expect(renderedRefreshReferenceData.find(ActionButton)).toHaveLength(1)
  })

  it('should call refreshReferenceData when refresh reference data button is pressed', () => {
    const wrapper = shallow(
      <TickerListView {...defaultProps} editMode={true} />
    )
    const sections = wrapper.find(SectionList).prop('sections')

    const refreshReferenceDataSection = sections[2]
    const renderedRefreshReferenceData = shallow(
      refreshReferenceDataSection.renderItem({ item: 1 })
    )
    renderedRefreshReferenceData.find(ActionButton).simulate('press')
    expect(refreshReferenceData).toHaveBeenCalled()
  })

  it('should call refresh on pull-to-refresh', () => {
    const wrapper = shallow(<TickerListView {...defaultProps} />)
    wrapper.find(SectionList).simulate('refresh')
    expect(refresh).toHaveBeenCalled()
  })
})
