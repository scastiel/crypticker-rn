import React from 'react'
import { AvailableTickerItem } from '../AvailableTickerItem'
import { shallow } from 'enzyme'
import { textAt } from '../../testHelpers'
import { Platform } from 'react-native'

const ticker = {
  id: 'XXBTZUSD',
  currFrom: {
    id: 'XXBT',
    symbol: 'BTC'
  },
  currTo: {
    id: 'ZUSD',
    symbol: 'USD'
  }
}
const addTickerAndRefresh = jest.fn()

const defaultProps = {
  ticker,
  addTickerAndRefresh
}

describe('Component: AvailableTickerItem', () => {
  beforeEach(() => {
    addTickerAndRefresh.mockClear()
  })

  it('should display ticker symbol and action button', () => {
    const wrapper = shallow(<AvailableTickerItem {...defaultProps} />)
    expect(textAt(wrapper, '.ticker-symbol')).toEqual('BTC → USD')
    expect(wrapper.find('.add-button')).toHaveLength(1)
  })

  it('should display ticker symbol and action button (Platform Android)', () => {
    Platform.OS = 'android'
    const wrapper = shallow(<AvailableTickerItem {...defaultProps} />)
    expect(textAt(wrapper, '.ticker-symbol')).toEqual('BTC → USD')
    expect(wrapper.find('.add-button')).toHaveLength(1)
  })

  it('should call addTickerAndRefresh when add button is clicked', () => {
    const wrapper = shallow(<AvailableTickerItem {...defaultProps} />)
    wrapper.find('.add-button').simulate('press')
    expect(addTickerAndRefresh).toHaveBeenCalledWith(ticker.id)
  })
})
