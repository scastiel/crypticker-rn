import React from 'react'
import { TickerItem } from '../TickerItem'
import Ticker from '../Ticker'
import { shallow } from 'enzyme'

const ticker = {
  id: 'XXBTZUSD',
  currFrom: 'XXBT',
  currTo: 'ZUSD'
}
const removeTicker = jest.fn()
const moveUpTicker = jest.fn()
const moveDownTicker = jest.fn()

const defaultProps = {
  ticker,
  removeTicker,
  moveUpTicker,
  moveDownTicker
}

describe('Component: Ticker', () => {
  beforeEach(() => {
    removeTicker.mockClear()
    moveUpTicker.mockClear()
    moveDownTicker.mockClear()
  })

  it('should display a ticker when not edit mode', () => {
    const wrapper = shallow(<TickerItem {...defaultProps} />)
    expect(wrapper.find(Ticker).prop('ticker')).toBe(ticker)
    expect(wrapper.find('.remove-button')).toHaveLength(0)
    expect(wrapper.find('.move-up-button')).toHaveLength(0)
    expect(wrapper.find('.move-down-button')).toHaveLength(0)
  })

  it('should display a ticker in edit mode (!canMoveUp, !canMoveDown)', () => {
    const wrapper = shallow(<TickerItem {...defaultProps} editMode={true} />)
    expect(wrapper.find(Ticker).prop('ticker')).toBe(ticker)
    expect(wrapper.find('.remove-button')).toHaveLength(1)
    expect(wrapper.find('.move-up-button')).toHaveLength(0)
    expect(wrapper.find('.move-down-button')).toHaveLength(0)
  })

  it('should display a ticker in edit mode (canMoveUp, !canMoveDown)', () => {
    const wrapper = shallow(
      <TickerItem {...defaultProps} editMode={true} canMoveUp={true} />
    )
    expect(wrapper.find(Ticker).prop('ticker')).toBe(ticker)
    expect(wrapper.find('.remove-button')).toHaveLength(1)
    expect(wrapper.find('.move-up-button')).toHaveLength(1)
    expect(wrapper.find('.move-down-button')).toHaveLength(0)
  })

  it('should display a ticker in edit mode (!canMoveUp, canMoveDown)', () => {
    const wrapper = shallow(
      <TickerItem {...defaultProps} editMode={true} canMoveDown={true} />
    )
    expect(wrapper.find(Ticker).prop('ticker')).toBe(ticker)
    expect(wrapper.find('.remove-button')).toHaveLength(1)
    expect(wrapper.find('.move-up-button')).toHaveLength(0)
    expect(wrapper.find('.move-down-button')).toHaveLength(1)
  })

  it('should display a ticker in edit mode (canMoveUp, canMoveDown)', () => {
    const wrapper = shallow(
      <TickerItem
        {...defaultProps}
        editMode={true}
        canMoveUp={true}
        canMoveDown={true}
      />
    )
    expect(wrapper.find(Ticker).prop('ticker')).toBe(ticker)
    expect(wrapper.find('.remove-button')).toHaveLength(1)
    expect(wrapper.find('.move-up-button')).toHaveLength(1)
    expect(wrapper.find('.move-down-button')).toHaveLength(1)
  })

  it('should call removeTicker when remove button is pressed', () => {
    const wrapper = shallow(<TickerItem {...defaultProps} editMode={true} />)
    wrapper.find('.remove-button').simulate('press')
    expect(removeTicker).toHaveBeenCalledWith(ticker.id)
  })

  it('should call moveUpTicker when move up button is pressed', () => {
    const wrapper = shallow(
      <TickerItem {...defaultProps} editMode={true} canMoveUp={true} />
    )
    wrapper.find('.move-up-button').simulate('press')
    expect(moveUpTicker).toHaveBeenCalledWith(ticker.id)
  })

  it('should call moveDownTicker when move down button is pressed', () => {
    const wrapper = shallow(
      <TickerItem {...defaultProps} editMode={true} canMoveDown={true} />
    )
    wrapper.find('.move-down-button').simulate('press')
    expect(moveDownTicker).toHaveBeenCalledWith(ticker.id)
  })
})
