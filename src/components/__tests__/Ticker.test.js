import React from 'react'
import { Ticker } from '../Ticker'
import { shallow } from 'enzyme'
import { textAt } from '../../testHelpers'
import { Platform } from 'react-native'

const ticker = {
  id: 'XXBTZUSD',
  currFrom: {
    id: 'XXBT',
    symbol: 'BTC'
  },
  currTo: {
    id: 'ZUSD',
    symbol: 'USD'
  },
  value: 1234.5
}

const platform = { os: 'ios' }

describe('Component: Ticker', () => {
  it('should display a ticker with value', () => {
    const wrapper = shallow(<Ticker ticker={ticker} platform={platform} />)
    expect(textAt(wrapper, '.currency-from-symbol')).toEqual('BTC')
    expect(textAt(wrapper, '.currency-to-symbol')).toEqual('USD')
    expect(textAt(wrapper, '.ticker-value')).toEqual('1,230')
  })

  it('should display a ticker with value (Platform Android)', () => {
    Platform.OS = 'android'
    const wrapper = shallow(
      <Ticker ticker={ticker} platform={{ os: 'android' }} />
    )
    expect(textAt(wrapper, '.ticker-value')).toEqual('1230')
  })

  it('should display a ticker with value (Platform Android), small numbers', () => {
    Platform.OS = 'android'
    const tickerWithBigValue = {
      ...ticker,
      value: 0.00012345
    }
    const wrapper = shallow(
      <Ticker ticker={tickerWithBigValue} platform={{ os: 'android' }} />
    )
    expect(textAt(wrapper, '.ticker-value')).toEqual('0.000123')
  })

  it('should display "…" as value if no ticker value', () => {
    const tickerWithNoValue = { ...ticker, value: undefined }
    const wrapper = shallow(
      <Ticker ticker={tickerWithNoValue} platform={platform} />
    )
    expect(textAt(wrapper, '.ticker-value')).toEqual('…')
  })
})
