import { createAction } from 'redux-actions'

export const removeTicker = createAction('REMOVE_TICKER')
export const moveUpTicker = createAction('MOVE_UP_TICKER')
export const moveDownTicker = createAction('MOVE_DOWN_TICKER')
export const addTicker = createAction('ADD_TICKER')
export const addTickers = createAction('ADD_TICKERS')
