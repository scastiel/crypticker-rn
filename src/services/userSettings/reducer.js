import { handleActions } from 'redux-actions'
import * as actions from './actions'

const defaultState = { selectedTickers: [] }

export default handleActions(
  {
    [actions.removeTicker]: (state, { payload: tickerIdToRemove }) => ({
      ...state,
      selectedTickers: state.selectedTickers.filter(
        tickerId => tickerId !== tickerIdToRemove
      )
    }),
    [actions.moveUpTicker]: (state, { payload: tickerIdToMove }) => {
      const index = state.selectedTickers.indexOf(tickerIdToMove)
      return {
        ...state,
        selectedTickers: switchItemsInArray(
          state.selectedTickers,
          index,
          index - 1
        )
      }
    },
    [actions.moveDownTicker]: (state, { payload: tickerIdToMove }) => {
      const index = state.selectedTickers.indexOf(tickerIdToMove)
      return {
        ...state,
        selectedTickers: switchItemsInArray(
          state.selectedTickers,
          index,
          index + 1
        )
      }
    },
    [actions.addTicker]: (state, { payload: tickerId }) => ({
      ...state,
      selectedTickers: [...state.selectedTickers, tickerId]
    }),
    [actions.addTickers]: (state, { payload: tickersIds }) => ({
      ...state,
      selectedTickers: tickersIds
    })
  },
  defaultState
)

// Helper functions

const switchItemsInArray = (array, index1, index2) =>
  array.map(
    (element, index) =>
      array[index === index1 ? index2 : index === index2 ? index1 : index]
  )
