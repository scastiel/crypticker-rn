import * as actions from './actions'
import { takeEvery, put, call } from 'redux-saga/effects'
import { Platform } from 'react-native'

function* initPlatform() {
  const platform = yield call(getPlatform)
  yield put(actions.definePlatform(platform))
}

export default function*() {
  yield takeEvery(actions.initPlatform, initPlatform)
}

// Helper functions

function getPlatform() {
  return { os: Platform.OS }
}
