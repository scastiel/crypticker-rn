import { handleActions } from 'redux-actions'
import * as actions from './actions'

const defaultState = {}

export default handleActions(
  {
    [actions.definePlatform]: (state, { payload: platform }) => ({
      ...state,
      ...platform
    })
  },
  defaultState
)
