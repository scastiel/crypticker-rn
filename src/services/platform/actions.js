import { createAction } from 'redux-actions'

export const initPlatform = createAction('INIT_PLATFORM')
export const definePlatform = createAction('DEFINE_PLATFORM')
