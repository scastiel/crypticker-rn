import { put, call, select } from 'redux-saga/effects'
import { refresh, __helpers } from '../sagas'
import { requestTickers, receiveTickers } from '../actions'
import { fetchTickers } from '../../../helpers/krakenAPI'

describe('Service: values > Sagas', () => {
  describe('refresh', () => {
    it('should not do anything if already refreshing', () => {
      const gen = refresh()

      expect(gen.next().value).toEqual(select(__helpers.isRefreshing))

      expect(gen.next(true).value).toEqual(put(requestTickers()))

      expect(gen.next().done).toBe(true)
    })

    it('should dispatch the right actions', () => {
      const gen = refresh()

      expect(gen.next().value).toEqual(select(__helpers.isRefreshing))

      expect(gen.next(false).value).toEqual(put(requestTickers()))

      expect(gen.next().value).toEqual(select(__helpers.getSelectedTickers))

      const selectedTickers = ['XXBTZUSD']
      expect(gen.next(selectedTickers).value).toEqual(
        call(fetchTickers, selectedTickers)
      )

      const tickers = { XXBTZUSD: 1234.5 }
      expect(gen.next(tickers).value).toEqual(
        call(__helpers.getCurrentDateISOString)
      )

      const date = '2017-11-24T01:48:21.123Z'
      expect(gen.next(date).value).toEqual(
        put(receiveTickers({ tickers, date }))
      )

      expect(gen.next().done).toBe(true)
    })
  })
})
