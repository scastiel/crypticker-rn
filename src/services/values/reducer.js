import { handleActions } from 'redux-actions'
import * as actions from './actions'

const defaultState = {
  requestingTickers: false,
  isManual: false,
  lastUpdated: null,
  tickers: {}
}

export default handleActions(
  {
    [actions.requestTickers]: (state, { payload: isManual }) => ({
      ...state,
      requestingTickers: true,
      isManual
    }),
    [actions.requestTickersFailed]: state => ({
      ...state,
      requestingTickers: false,
      isManual: false
    }),
    [actions.receiveTickers]: (state, { payload: { tickers, date } }) => ({
      ...state,
      lastUpdated: date,
      requestingTickers: false,
      isManual: false,
      tickers
    })
  },
  defaultState
)
