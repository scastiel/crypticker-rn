import { createAction } from 'redux-actions'

export const requestTickers = createAction('REQUEST_TICKERS')
export const receiveTickers = createAction('RECEIVE_TICKERS')
export const refresh = createAction('REFRESH')
export const requestTickersFailed = createAction('REQUEST_TICKERS_FAILED')
