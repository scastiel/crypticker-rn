import * as actions from './actions'
import { delay } from 'redux-saga'
import { takeEvery, put, select, call } from 'redux-saga/effects'
import { fetchTickers } from '../../helpers/krakenAPI'
import { showError, hideError } from '../gui/actions'

export function* refresh({ payload: isManual } = {}) {
  const isAlreadyRefreshing = yield select(isRefreshing)

  yield put(actions.requestTickers(isManual))

  if (isAlreadyRefreshing) {
    return
  }

  const selectedTickers = yield select(getSelectedTickers)
  try {
    const tickers = yield call(fetchTickers, selectedTickers)
    const currentDateISOString = yield call(getCurrentDateISOString)
    yield put(actions.receiveTickers({ tickers, date: currentDateISOString }))
  } catch (err) {
    yield put(actions.requestTickersFailed())
    yield* showAndHideError('Error while fetching tickers.')
  }
}

export default function*() {
  yield takeEvery(actions.refresh, refresh)
}

// Helper functions

function getSelectedTickers(state) {
  return state.userSettings.selectedTickers
}

function getCurrentDateISOString() {
  return new Date().toISOString()
}

function* showAndHideError(errorMessage) {
  yield put(showError(errorMessage))
  yield delay(5000)
  yield put(hideError())
}

function isRefreshing(state) {
  return state.values.requestingTickers
}

export const __helpers = {
  getSelectedTickers,
  getCurrentDateISOString,
  isRefreshing
}
