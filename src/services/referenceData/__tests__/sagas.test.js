import { put, call } from 'redux-saga/effects'
import { getCurrenciesAndTickers } from '../sagas'
import {
  requestCurrenciesAndAvailableTickers,
  receiveCurrenciesAndAvailableTickers
} from '../actions'
import { refresh } from '../../values/actions'
import { fetchCurrenciesAndAvailableTickers } from '../../../helpers/krakenAPI'

describe('Service: referenceData > Sagas', () => {
  describe('getCurrenciesAndTickers', () => {
    it('should dispatch the right actions', () => {
      const gen = getCurrenciesAndTickers()

      expect(gen.next().value).toEqual(
        put(requestCurrenciesAndAvailableTickers())
      )

      expect(gen.next().value).toEqual(call(fetchCurrenciesAndAvailableTickers))

      const currencies = []
      const tickers = []
      expect(gen.next({ currencies, tickers }).value).toEqual(
        put(receiveCurrenciesAndAvailableTickers({ currencies, tickers }))
      )

      expect(gen.next().value).toEqual(put(refresh()))
    })
  })
})
