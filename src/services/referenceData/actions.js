import { createAction } from 'redux-actions'

export const refreshReferenceData = createAction('REFRESH_REFERENCE_DATA')
export const getCurrenciesAndTickers = createAction(
  'GET_CURRENCIES_AND_TICKERS'
)
export const requestCurrenciesAndAvailableTickers = createAction(
  'REQUEST_CURRENCIES_AND_AVAILABLE_TICKERS'
)
export const receiveCurrenciesAndAvailableTickers = createAction(
  'RECEIVE_CURRENCIES_AND_AVAILABLE_TICKERS'
)
