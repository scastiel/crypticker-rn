import { takeEvery, put, call } from 'redux-saga/effects'
import * as actions from './actions'
import { refresh } from '../values/actions'
import sortBy from 'lodash/sortBy'
import { delay } from 'redux-saga'
import { fetchCurrenciesAndAvailableTickers } from '../../helpers/krakenAPI'
import { showError, hideError } from '../gui/actions'

export function* getCurrenciesAndTickers() {
  yield put(actions.requestCurrenciesAndAvailableTickers())
  try {
    const { currencies, availableTickers } = yield call(
      fetchCurrenciesAndAvailableTickers
    )

    yield put(
      actions.receiveCurrenciesAndAvailableTickers({
        currencies,
        tickers: sortTickers(availableTickers)
      })
    )

    yield put(refresh())
  } catch (err) {
    yield* showAndHideError(
      'Error while fetching currencies and tickers data. Please check your internet connection then reload the app.'
    )
  }
}

export default function*() {
  yield takeEvery(actions.getCurrenciesAndTickers, getCurrenciesAndTickers)
  yield takeEvery(actions.refreshReferenceData, getCurrenciesAndTickers)
}

// Helper functions

function sortTickers(tickers) {
  return sortBy(tickers, [
    t => t.currFrom !== 'XXBT',
    'currFrom',
    t => t.currTo !== 'XXBT',
    'currTo'
  ])
}

function* showAndHideError(errorMessage) {
  yield put(showError(errorMessage))
  yield delay(5000)
  yield put(hideError())
}
