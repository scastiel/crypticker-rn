import { handleActions } from 'redux-actions'
import * as actions from './actions'

export const CURRENT_REFERENCE_DATA_VERSION = 2

const defaultState = {
  currencies: [],
  tickers: [],
  version: CURRENT_REFERENCE_DATA_VERSION
}

export default handleActions(
  {
    [actions.receiveCurrenciesAndAvailableTickers]: (
      state,
      { payload: { tickers, currencies } }
    ) => ({
      ...state,
      currencies,
      tickers
    })
  },
  defaultState
)
