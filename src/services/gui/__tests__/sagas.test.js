import { periodicRefresh } from '../actions'
import { addTicker } from '../../userSettings/actions'
import { refresh } from '../../values/actions'
import { delay } from 'redux-saga'
import { put, call } from 'redux-saga/effects'
import * as sagas from '../sagas'

describe('Service: gui > Sagas', () => {
  describe('addTickerAndRefresh', () => {
    it('should call addTicker action then refresh', () => {
      const gen = sagas.addTickerAndRefresh({ payload: '1234' })
      expect(gen.next().value).toEqual(put(addTicker('1234')))
      expect(gen.next().value).toEqual(put(refresh()))
      expect(gen.next().done).toBe(true)
    })
  })

  describe('periodicRefresh', () => {
    it('should call refresh then call itself after 60 seconds', () => {
      const gen = sagas.periodicRefresh()
      expect(gen.next().value).toEqual(put(refresh()))
      expect(gen.next().value).toEqual(
        call(delay, sagas.PERIODIC_REFRESH_INTERVAL_MILLISECONDS)
      )
      expect(gen.next().value).toEqual(put(periodicRefresh()))
      expect(gen.next().done).toBe(true)
    })
  })
})
