import { handleActions } from 'redux-actions'
import * as actions from './actions'

const defaultState = {
  editMode: false,
  errorDisplayed: false,
  errorMessage: null
}

export default handleActions(
  {
    [actions.activateEditMode]: state => ({
      ...state,
      editMode: true
    }),
    [actions.deactivateEditMode]: state => ({
      ...state,
      editMode: false
    }),
    [actions.showError]: (state, { payload: errorMessage }) => ({
      ...state,
      errorDisplayed: true,
      errorMessage: errorMessage
    }),
    [actions.hideError]: state => ({
      ...state,
      errorDisplayed: false,
      errorMessage: null
    })
  },
  defaultState
)
