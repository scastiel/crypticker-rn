import { createAction } from 'redux-actions'

export const activateEditMode = createAction('ACTIVATE_EDIT_MODE')
export const deactivateEditMode = createAction('DEACTIVATE_EDIT_MODE')
export const addTickerAndRefresh = createAction('ADD_TICKER_AND_REFRESH')
export const deactivateEditModeAndRefresh = createAction(
  'DEACTIVATE_EDIT_MODE_AND_REFRESH'
)
export const periodicRefresh = createAction('PERIODIC_REFRESH')
export const hideError = createAction('HIDE_ERROR')
export const showError = createAction('SHOW_ERROR')
