import { addTicker } from '../userSettings/actions'
import { refresh } from '../values/actions'
import * as actions from './actions'
import { delay } from 'redux-saga'
import { takeEvery, put, call } from 'redux-saga/effects'

export const PERIODIC_REFRESH_INTERVAL_MILLISECONDS = 60000

export function* addTickerAndRefresh({ payload: tickerId }) {
  yield put(addTicker(tickerId))
  yield put(refresh())
}

export function* periodicRefresh() {
  yield put(refresh())
  yield call(delay, PERIODIC_REFRESH_INTERVAL_MILLISECONDS)
  yield put(actions.periodicRefresh())
}

export default function*() {
  yield takeEvery(actions.addTickerAndRefresh, addTickerAndRefresh)
  yield takeEvery(actions.periodicRefresh, periodicRefresh)
}
